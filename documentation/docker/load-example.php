<?php
// This file was originally copied from the directory
//   /documentation/docker/load-example.php
// And it's intended to be just saved as:
//   /load.php


// Show errors on screen so developers are happy.
error_reporting(E_ALL);
ini_set('display_errors', 1);

// Connect to the "db" host running a MariaDB instance.
// This is available thanks your docker-compose.yaml
$database = 'ldto';
$username = 'ldto';
$password = 'changemeasd!';
$location = 'db';
$charset  = 'utf8mb4';
$prefix   = 'ldto_';

// Extra configurations that are specific to your Linux Day website.
define('DB_TIMEZONE', 'Europe/Rome');
define('CONTACT_EMAIL', 'asd@asd.asd');
define('CONTACT_PHONE', '555-555-555');

// Configurations for the framework "suckless-php".
// https://gitpull.it/w/suckless-php/
define('DEBUG', true);
define('ABSPATH', __DIR__);
define('ROOT', '');
define('REQUIRE_LOAD_POST', __DIR__ . '/includes/load-post.php' );
require                     __DIR__ . '/includes/suckless-php/load.php';

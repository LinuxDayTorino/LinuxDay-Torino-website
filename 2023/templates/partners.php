<?php
# Linux Day Torino Website
# Copyright (C) 2016-2023 Valerio Bozzolan, Linux Day Torino website contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This file contains some partners / supporters / sponsors.
 */

// Do not allow to visit this file directly to avoid confusing things.
if( !defined( 'ABSPATH' ) ) {
	exit;
}

?>

<div class="row partners">
	<div class="col-12-small col-4-xlarge">
		<p><?= __( "Con il supporto attivo di:" ) ?></p>

		<div class="row">
			<div class="col s12 m4 l2">
				<a href="https://www.ils.org/" target="_blank">
					<img src="<?= CURRENT_CONFERENCE_PATH ?>/images/libre/ils-logo-circle-text.svg" alt="Italian Linux Society" />
				</a>
			</div>
			<div class="col s12 m4 l2">
				<a href="https://www.linuxday.it/" target="_blank">
					<img src="<?= CURRENT_CONFERENCE_PATH ?>/images/libre/linuxday_fullcolor.svg" alt="Linux Day Italia" />
				</a>
			</div>
		</div>
	</div>

	<div class="col-12-small col-4-xlarge">
		<p><?= __( "Con il patrocinio di:" ) ?></p>

		<div class="row">
			<div class="col s12 m4 l2">
				<a href="http://www.comune.torino.it/" target="_blank">
					<img src="<?= CURRENT_CONFERENCE_PATH ?>/images/partner/citta-torino.jpg" alt="<?= esc_attr( __( "Città di Torino" ) ) ?>" />
				</a>
			</div>
			<div class="col s12 m4 l2">
				<a href="https://www.unito.it/" target="_blank">
					<img src="<?= CURRENT_CONFERENCE_PATH ?>/images/partner/unito-logo-300.png" alt="<?= esc_attr( __( "Università di Torino" ) ) ?>" />
				</a>
			</div>
		</div>
	</div>

	<div class="col-12-small col-4-xlarge">
		<p><?= __( "Con la collaborazione di:" ) ?></p>

		<div class="row">
			<div class="col s12 m4 l2">
				<a href="https://www.mupin.it/" target="_blank">
					<img src="<?= CURRENT_CONFERENCE_PATH ?>/images/partner/mupin-logo-300.png" alt="<?= esc_attr( __( "Museo Piemontese dell'Informatica" ) ) ?>" />
				</a>
			</div>
		</div>
	</div>
</div>

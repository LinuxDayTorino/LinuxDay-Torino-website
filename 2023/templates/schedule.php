<table class="schedule">
    <thead>
    <tr>
        <th width="10%"><?= __("Time") ?></th>
        <th width="22.5%">BASE</th>
        <th width="22.5%">SYS</th>
        <th width="22.5%">DEV</th>
        <th width="22.5%">MISC</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <th>14:00</th>
        <td>
            <b>Primi passi per Linux</b> <br> Roberto Guido <br> <a href="<?= CURRENT_CONFERENCE_PATH ?>/assets/slides/PrimiPassiConLinux.pdf">Scarica Slides</a>
        </td>
        <td>
            <b>systemd, un amico prezioso</b> <br> Giuseppe Sacco <br> <a href="<?= CURRENT_CONFERENCE_PATH ?>/assets/slides/LinuxDay2023_systemd.pdf">Scarica Slides</a>
        </td>
        <td>
            <b>Rust</b> <br> Luca Barbato
        </td>
        <td>
            <b>LocalAI: L'alternativa open a OpenAI</b> <br> Ettore Di Giacinto <br> <a href="<?= CURRENT_CONFERENCE_PATH ?>/assets/slides/LocalAI-Linux Day - Torino.pdf">Scarica Slides</a>
        </td>
    </tr>
    <tr>
        <th>15:00</th>
        <td>
            <b>Quali sono i rischi dell'intelligenza artificiale e cosa possiamo fare per prevenirli?</b> <br> Stefania Delprete <br> <a href="<?= CURRENT_CONFERENCE_PATH ?>/assets/slides/LinuxDay2023_Torino_Delprete_AI_Safety.pdf">Scarica Slides</a>
        </td>
        <td>
            <b>Storage, anno 2023, dalle basi al futuro</b> <br> Massimo Nuvoli <br> <a href="<?= CURRENT_CONFERENCE_PATH ?>/assets/slides/LD2023_storage.pdf">Scarica Slides</a>
        </td>
        <td>
            <b>Postgres: Introduzione alla ORM</b> <br> Davide Morra <br> <a href="<?= CURRENT_CONFERENCE_PATH ?>/assets/slides/LINUX DAY - TORINO 2023 - Postgres Primi passi tramite ORM.pdf">Scarica Slides</a>
        </td>
        <td>
            <b>Odoo - Digitalizzazione ai tempi dell'open source</b> <br> Marco Rotella <br> <a href="<?= CURRENT_CONFERENCE_PATH ?>/assets/slides/linux-day-23.odp">Scarica Slides</a>
        </td>
    </tr>
    <tr>
        <th>16:00</th>
        <td>
            <b>Introduzione a Wikidata</b> <br> Elena Marangoni <br> <a href="<?= CURRENT_CONFERENCE_PATH ?>/assets/slides/wikidata.pdf">Scarica Slides</a>
        </td>
        <td>
            <b>Configuriamo un centralino VoIP open-source</b> <br> Andrea Mannarella <br> <a href="<?= CURRENT_CONFERENCE_PATH ?>/assets/slides/asterisk.pdf">Scarica Slides</a>
        </td>
        <td>
            <b>B-AROL-O Team: Open Source Hardware and Software for fun</b> <br> Gianpaolo Macario <br> <a href="<?= CURRENT_CONFERENCE_PATH ?>/assets/slides/B-AROL-O.Team.-.Presentation.to.Linux.Day.Torino.2023-10-28.pdf">Scarica Slides</a>
        </td>
        <td>
            <b>Animali elettronici da compagnia: Quale futuro?</b> <br> Gianfranco Poncini <br> <a href="<?= CURRENT_CONFERENCE_PATH ?>/assets/slides/Linux_Day_Torino_2023_Animali_elettronici_da_compagnia.pdf">Scarica Slides</a>
        </td>
    </tr>
    <tr>
        <th>17:00</th>
        <td>
            <b>ILIAS LMS</b> <br> Vincenzo Padula <br> <a href="<?= CURRENT_CONFERENCE_PATH ?>/assets/slides/linux-day-ottobre-2023.odp">Scarica Slides</a>
        </td>
        <td>
            <b>Git!</b> <br> Davide Maietta
        </td>
        <td>
            <b>Aprire porte e gestire accessi in open source</b> <br> Matteo Giaccone <br> <a href="<?= CURRENT_CONFERENCE_PATH ?>/assets/slides/Linux day Torino 2023 - Aprire porte e gestire accessi in open source.pdf">Scarica Slides</a>
        </td>
        <td>
            <b>Flipper Zero, il coltellino svizzero, con i bit</b> <br> Francesco Tucci <br> <a href="<?= CURRENT_CONFERENCE_PATH ?>/assets/slides/Flipper Zero.pdf">Scarica Slides</a>
        </td>
    </tr>
    </tbody>
</table>

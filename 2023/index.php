<?php
# Linux Day Torino Website
# Copyright (C) 2016-2023 Valerio Bozzolan, Linux Day Torino website contributors
# Copyright (C) 2023      Rosario Antoci, Linux Day Torino website contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This file is the homepage of the website.
 */

// Load the framework and configs.
require 'load.php';

// Embed the HTML header.
template( 'header' );

$program_url = CURRENT_CONFERENCE_PATH . '/program/';
$program_url = keep_url_in_language( $program_url );
$program_url_safe = esc_attr( $program_url );

// Start body of the page.
?>

	<div class="inner">
		<header>
			<img src="images/ldto23.svg" class="image right">
			<h1><?= __( "Sabato 28 Ottobre <br>c/o Dipartimento di Informatica UniTO" ) ?></h1>
		</header>

		<section>
			<p>Il Linux Day Torino è l'evento annuale dedicato a tutti gli appassionati, sviluppatori e professionisti del mondo Linux e dell'open source. È l'opportunità perfetta per condividere conoscenze, esperienze e progetti, e per ampliare la tua rete di contatti nel settore.</p>
		</section>

		<section>
			<h2><?= __( "Location") ?></h2>
			<div class="row">
				<div class="col-12-xsmall col-6-xlarge">
					<iframe width="100%" height="350" src="https://www.openstreetmap.org/export/embed.html?bbox=7.655201554298402%2C45.08829703318932%2C7.664160132408143%2C45.0917661553084&amp;layer=mapnik&amp;marker=45.090031620587375%2C7.6596808433532715" style="border: 1px solid black"></iframe>
					<br/><small><a href="https://www.openstreetmap.org/?mlat=45.09003&amp;mlon=7.65968#map=18/45.09003/7.65968">View Larger Map</a></small>
				</div>
				<div class="col-12-xsmall col-6-xlarge">
					<p><?= __( "Il Linux Day Torino 2023 si svolge al Dipartimento di Informatica dell'Università, in Via Pessinetto 12." ) ?></p>
					<p><?= sprintf(
						__( "Puoi prendere il tram n°9 e n°3, scendendo alla fermata %s" ),
						__( "Ospedale Amedeo di Savoia / Dipartimento di Informatica.")
					) ?></p>
					<p><?= sprintf(
						__( "Dalla fermata della metropolitana %s puoi prendere il pullman n°%s scendendo alla fermata %s." ),
						__( "XVIII Dicembre" ),
							"59",
						__( "Svizzera" )
					) ?></p>
				</div>
			</div>
		</section>

		<section>
			<h2>Programma</h2>
			<!-- Table wrapped in a div to add horizontal scroll bar in smaller screen -->
			<div style="overflow-x: auto">
				<?php template( 'schedule' ) ?>
			</div>

			<div class="tiles">
				<article class="style1">
					<span class="image">
						<img src="images/lip.jpg" alt="">
					</span>
					<div>
						<h2>Linux Install Party</h2>
						<div class="content">
							<p>Porta il tuo PC e installiamo Linux insieme!</p>
						</div>
					</div>
				</article>
				<article class="style2">
					<span class="image">
						<img src="images/restart_anto.jpg" alt="">
					</span>
					<div>
						<h2>Restart Party</h2>
						<div class="content">
							<p>Hai un oggetto da riparare? Portalo e gli diamo una occhiata!</p>
						</div>
					</div>
				</article>
				<article class="style3">
					<span class="image">
						<img src="images/distribuzione-t-shirts.jpg" alt="">
					</span>
					<div>
						<h2>Gadgets</h2>
						<div class="content">
							<p>Stickers, opuscoli, e le oramai mitiche magliette del Linux Day Torino!</p>
						</div>
					</div>
				</article>
			</div>
		</section>

		<section class="partners">
			<?php template( 'partners' ) ?>
		</section>
	</div>

<?php
// End body of the page.

// Embed the HTML footer.
template( 'footer' );

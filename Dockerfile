FROM debian:bullseye

RUN  apt-get update \
  && DEBIAN_FRONTEND=noninteractive apt-get install -y \
	apache2 \
	libapache2-mod-php \
	php-mbstring \
	php-xml \
	php-mysql \
	libjs-jquery \
	libjs-leaflet \
	libmarkdown-php \
	gettext \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

RUN  a2enmod rewrite \
  && a2enconf javascript-common \
  && echo "ServerName ldto" >> /etc/apache2/sites-enabled/000-default.conf \
  && sed --regexp-extended --in-place 's@^ *([[:space:]]*)ErrorLog .*error.log$@\1ErrorLog /dev/stderr@' /etc/apache2/apache2.conf /etc/apache2/sites-enabled/000-default.conf \
  && sed --regexp-extended --in-place 's@^([[:space:]]*)CustomLog .*access.log.*@\1CustomLog /dev/stdout combined@' /etc/apache2/sites-enabled/000-default.conf \
  && echo                             >> /etc/apache2/sites-enabled/000-default.conf \
  && echo "<Directory /var/www/html>" >> /etc/apache2/sites-enabled/000-default.conf \
  && echo "	AllowOverride All"    >> /etc/apache2/sites-enabled/000-default.conf \
  && echo "</Directory>"              >> /etc/apache2/sites-enabled/000-default.conf

CMD ["/usr/sbin/apachectl", "-D", "FOREGROUND"]

WORKDIR /var/www/html

EXPOSE 80
